<?php

	  // get historical start date
	  function getStartDate($dr,$enddate){
		
		$endDateArray = explode('/',$enddate);
		
		$year = $endDateArray[2];
		$month= $endDateArray[0];
		$day = $endDateArray[1];
		

		$month = $month - $dr;

		if($month > -12 && $month <= 0){
			$month = 12 + $month;
			$year = $year -1;
		} else if($month > -24 && $month <= -12){
			$month = 24 + $month;
			$year = $year -2;
		}

				
		//==== get previous day ==================
		
		if($day == 1){
			switch($month){
				case 5:
				case 7:
				case 8:
				case 10:
				case 12:
					$day = 30;
					$month = $month - 1;
					break;
				case 2:
				case 4:
				case 6:
				case 9:
				case 11:
					$day = 31;
					$month = $month - 1;
					break;
				case 3:
					$day = 28;
					$month = $month - 1;
					break;
				case 1:
					$day = 31;
					$month = 12;
					$year = $year - 1;
					break;
			}
		} else {
			$day = $day - 1;
		}
		
		//====================================
		$month = $month -1;
		
		$startString = "";
		$startString .= "&a=" . getTwoNumberType($month);
		$startString .= "&b=" . getTwoNumberType($day);
		$startString .= "&c=" . $year;

		return $startString;
	  }

	  // get historical end date
	  function getEndDate($enddate){
		$endDateArray = explode('/',$enddate);
				
		$year = $endDateArray[2];
		$month= $endDateArray[0];
		$day = $endDateArray[1];
		
		
		$month = $month -1;

		$endString = "";
		$endString .= '&d=' . getTwoNumberType($month);
		$endString .= '&e=' . getTwoNumberType($day);
		$endString .= '&f=' . $year;

		return $endString;
		
	  }

	  function getTwoNumberType($s){
		if(strlen($s) == 1){
			$s = "0" . $s;
		}
		return $s;
	  }


	$symbol = $_REQUEST['sid'];
	$symbol= strtoupper($symbol);
	
	$drange = $_REQUEST['drange'];
	
	$quoteurl = "http://finance.yahoo.com/d/quotes.csv?s="  . $symbol ."&f=d1" ;

	$quoteEndDate = file_get_contents($quoteurl);
	$quoteEndDate = explode('"',$quoteEndDate);
	$endDate = $quoteEndDate[1];
	
	$start = getStartDate($drange,$endDate);
	$end = getEndDate($endDate);

	$url = "http://ichart.finance.yahoo.com/table.csv?s=" . $symbol ;
	$url .= $start ;
	$url .= $end;
	$url .= "&g=d&ignore=.csv";
	
	echo file_get_contents($url);
?>