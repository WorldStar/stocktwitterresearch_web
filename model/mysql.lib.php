<?php
     /**
      * This class should be used to make manipulation
      * of MySQL database easier to programmer
      * @author Bojan Bijelic <bojan.bijelic.os@gmail.com>
      * @version 1.0
      */                             
     class MySQL {
          
          /**
           * Server, username, password
           * @access protected
           * @var array
           */                                           
          protected $connectionData;
          
          /**
           * Connection link identifier
           * @access private
           * @var bool|link
           */
          private $connection; 
          
          /**
           * Error string; Will be set if error occures
           * @access private
           * @var array
           */
          private $errorStack;      
          
          /**
           * Query result
           * @access private
           * @var bool|resource
           */
          private $queryResult;                                                                                                                          
          
          /**
           * Constructor
           * @param array $data Connection data
           * @access public
           */
          public function __construct( $hostName, $userName, $password ){
               // Set connection property to false by default
               // Neccessary for manual database selection
               $this -> connection = false;
               // Set error stack as array
               $this -> errorStack = array();
               // Initialize connection data property
               $this -> connectionData = array();
               // Check if given parameter is array
				$this -> connectionData["Server"] = $hostName;
				$this -> connectionData["User"] = $userName;
				$this -> connectionData["Password"] = $password;
				
          }                                           
          
          /**
           * Connect to MySQL server with this method
           * If database name is injected through parameter,
           * it will be selected upon successuful connection
           * If connection was successuful boolean true will be returned, 
           * otherwise boolean false will be returned                                            
           * @param string $dbName This is optional parameter of database name 
           * @access public
           * @return boolean           
           */
           public function connect( $dbName = "" ){
               // Connect to MySQL server
               $this -> connection = @mysql_connect( $this -> connectionData["Server"],
                                                     $this -> connectionData["User"],
                                                     $this -> connectionData["Password"] );

//				$this->query("SET NAMES SJIS");
				
               // Check if connection is open
               if ( $this -> connection == false ){
                    // Add error message
                    $this -> addError( "Unable to connect to MySQL server" );
                    // Return boolean false
                    return false;
               }
               // First check if provided parameter is empty
               if( $dbName != "" ){
                   // Select database
                   if( @mysql_select_db( $dbName ) == false )
                   // If failure, add error to stack 
                   $this -> addError( "Unable to select <em>" . $dbName . "</em> database" );  
               // If database in data array is not set
               } else {
                    // Check if database name is set in connection data property
                    if( isset( $this -> connectionData["Database"] ) ){
                         // If it is set indeed, then check if is it empty
                         if( $this -> connectionData["Database"] != "" ){
                             // Select database with provided data
                             if( @mysql_select_db( $this -> connectionData["Database"] ) == false )
                             // If failure, add error to stack 
                             $this -> addError( "Unable to select <em>" . $dbName . "</em> database" );
                         } 
                    }   
                }         
           }
           
           /**
            * This method executes query on database
            * @param string|array $queryString MySQL query string
            * @access public
            * @return boolean            
            */               
           public function query( $queryString ){
               // Check if query string is string and not empty
               if( is_string( $queryString ) && $queryString !="" ){
                    // Execute string
                    $this -> queryResult = @mysql_query( $queryString );
                    // If execution didnt went well...
                    if ( $this -> queryResult == false ){
                         // Add error message
                         $this -> addError( mysql_error() );
                         // Return boolean false
                         return false;
                    }
                    // Return boolean true
                    return true;
               // Otherwise, push error message into error stack
               // and return false
               } else {
                    // Add error message
                    $this -> addError("Query string is invalid");
                    // Return boolean false
                    return false;
               }   
           }      
           
           /**
            * This method counts and returns how many rows query reurned
            * @access public
            * @return int|boolean
            */
           public function rowsReturned(){
               // Check if result property is not false
               // If it is false it means that query was not executed
               if ( $this -> queryResult != false ){
                    // Return how many rows is returned by query
                    return (int)@mysql_num_rows( $this -> queryResult );
               // Otherwise, add error message and return false
               } else {
                    // Add error message
                    $this -> addError("Execute query before calling rowsReturned()");
                    // Return boolean false
                    return false;
               }
           }   
           
           /**
            * Get data fetched array on query execution
            * @access public                      
            * @return array
            */
           public function fetchArray(){
               // Check if result property is not false
               // If it is false it means that query was not executed
               if ( $this -> queryResult != false ){   
                    // Return how many rows is returned by query
                    $data = @mysql_fetch_assoc( $this -> queryResult );
                    return $data;
               // Otherwise, add error message and return false
               } else {
                    // Add error message
                    $this -> addError("To get data execute query fist");
                    // Return boolean false
                    return false;
               }    
           }                                                                                                                   
           
           public function fetchAll(){
               // Check if result property is not false
               // If it is false it means that query was not executed
               if ( $this -> queryResult != false ){   	
			   		$res = array();
					
                    // Return how many rows is returned by query
                    while($data = @mysql_fetch_assoc( $this -> queryResult ))
						array_push($res, $data);
						
                    return $res;
               // Otherwise, add error message and return false
               } else {
                    // Add error message
                    $this -> addError("To get data execute query fist");
                    // Return boolean false
                    return false;
               }    
           }                                                                                                                   
           
           /**
            * Get data fetched row on query execution
            * @access public                      
            * @return array
            */
           public function fetchRow(){
               // Check if result property is not false
               // If it is false it means that query was not executed
               if ( $this -> queryResult != false ){   
                    // Return how many rows is returned by query
                    $data = @mysql_fetch_row( $this -> queryResult );
                    return $data;
               // Otherwise, add error message and return false
               } else {
                    // Add error message
                    $this -> addError("To get data execute query fist");
                    // Return boolean false
                    return array();
               }    
           }                                                                                                                   
           
           /**
            * Manualy select database
            * @access public
            * @param string $dbName Database name
            */
           public function selectDatabase( $dbName ){
               // Check if there is open connection to database
               if( $this -> connection != false ){
                    // Check if database name provided in parameter is string and
                    // not empty
                    if( is_string( $dbName ) && $dbName !="" ){
                         // If database name is string and not empty
                         // Select database
                         if(!@mysql_select_db( $dbName )){
                              // On failure, add error to stack
                              $this -> addError( "Unable to select <em>" . $dbName . "</em> database" );                              
                         }     
                    // Otherwise addError
                    } else $this -> addError( "Database name provided is not valid" );
               // If connection is not opened, add error     
               } else $this -> addError( "Can not select <em>" . $dbName . "</em> database while connection is closed" ); 
           }                                               
           
           /**
            * Disconnect from database
            * @access public
            */
           public function disconnect(){
               // Close connection if connection property is valid
               if( $this -> connection != false ){
                    // Close connection
                    @mysql_close( $this -> connection );
               // else add error message
               } else $this -> addError("Connection is already closed");
           }                                   
           
           /**
            * Add error message to error stack
            * @access private
            * @param string|array $errorMsg Error message string
            */
           private function addError( $errorMsg ){
               // Check if error message provided in parameters is string
               if( is_string( $errorMsg ) ){
                    // If it's string, push it into error stack
                    array_push( $this -> errorStack, $errorMsg );
               } 
               // Check if error message is array
               if( is_array( $errorMsg ) ){
                    // If is array, foreach message in it, push it to the 
                    // error stack
                    foreach( $errorMsg as $error ){
                         // Push error message into error stack
                         array_push( $this -> errorStack, $error );
                    }
               }
           }  
           
           /**
            * With this function you can export all error
            * messages into one string and return it
            * @access public
            * @return string
            */
           public function getErrors(){
               // Check if error message array is empty
               if( count( $this -> errorStack ) > 0 ) {
                    // If error stack is not empty
                    // Foreach of errors, place them 
                    // into inside-method variable
                    $output = "";
                    // Foreach error message
                    foreach( $this -> errorStack as $error ){
                         // Append it to the output variable
                         $output .= $error . '<br /';
                    }
                    // Return output string
                    return $output;
               // If there is not messages in stack
               // return no-errors message
               } else return "There was no errors";   
           }                                                                                                                                                  
          
		  public function getInsertedId($tbName, $id_field) {
			  $query = "SELECT MAX($id_field) FROM $tbName";
			  
			  $this->query($query);
			  
			  $res = $this->fetchRow();
			  
			  return $res[0];
		  }
		  
     }

?>