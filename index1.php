<?php
session_start(); 

include_once("conf/definition.php");
include_once("model/mysql.lib.php");
include_once("controller/navigator.php");
include_once("controller/stockchart.php");
include_once("controller/exhandler.php");
//include_once("controller/usermng.php");
include_once("view/home.php");
include_once("view/login.php");
include_once("view/footer.php");
include_once("view/header.php");
include_once("view/profile.php");


?>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<title>TwitterStockResearch</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0">

<link rel="stylesheet" href="css/bootstrap.css">
<link rel="stylesheet" href="css/bootstrap-responsive.css">
<link rel="stylesheet" type="text/css" href="css/jquery.jqplot.css" />
<link rel="stylesheet" type="text/css" href="syntaxhighlighter/styles/shCoreDefault.css" />
<link rel="stylesheet" type="text/css" href="syntaxhighlighter/styles/shThemejqPlot.css" />
<link href="css/bootstrap-combined.no-icons.css" rel="stylesheet">
<link href="//netdna.bootstrapcdn.com/font-awesome/3.2.1/css/font-awesome.css" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="css/style.css">

<!-- Login -->
<link rel="stylesheet" type="text/css" href="./css/login/style.css" />
<link rel="stylesheet" type="text/css" href="./css/login/animate-custom.css" />
<!----------->

<!-- Dialog -->
<link rel="stylesheet" href="http://code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css" />
<!------------>

<?php
if(!empty($_POST) || isset($_SESSION['2376fa']))
	echo '<script src="js/jquery.js"></script>';
//print_r($_POST); die($_SESSION['login']);

?>

<script src="js/excanvas.js"></script>
<script src="js/jquery.jqplot.js"></script>

<script src="plugins/jqplot.dateAxisRenderer.js"></script>
<script src="plugins/jqplot.ohlcRenderer.js"></script>
<script src="plugins/jqplot.highlighter.js"></script>

<script src="syntaxhighlighter/scripts/shCore.js"></script>
<script src="syntaxhighlighter/scripts/shBrushJScript.js"></script>
<script src="syntaxhighlighter/scripts/shBrushXml.js"></script>

<script src="js/bootstrap.js"></script>
<script src="js/yui.js"></script>
<?
  //if(isset($_SESSION['2376fa']))
     echo '<script src="js/ys_autocomplete.js"></script>';
?>


<?php
//print_r($_REQUEST);print_r($_POST);

//if(empty($_POST) && isset($_SESSION['2376fa']) && !isset($_REQUEST['profile']))
if(!isset($_REQUEST['profile']))
	echo '
		<script src="js/yql_process.js"></script>
		<script src="js/chart_module.js"></script>
		';
?>
</head>
<body>


<?php

if(ExceptionHandler::neededToShowMsg())
	ExceptionHandler::showMsgDiag();

Header::show();

if(empty($_POST) && !isset($_SESSION['2376fa'])) {
	$_SESSION['l94jso28dn'] = "";
?>

<div class="container back-white">
	<div class="container-fluid">
            <!-- main content part -->
            	<form class="form-search" onsubmit="return false">
            	<div class="info-part">
                	<p class="subtitle">Stock, Index, ETF Symbol Info</p>
                    <div class="subcontents">
                        <div class="yui3-skin-sam"><!--DIV FOR AUTOCOMPLETE-->
                            <input type="text" id="txtSymbol" class="input-medium" value="YHOO" name="txtSymbol" placeholder="Symbol" autocomplete="off">
							<button type="submit" id="btnScan" class="btn btn-info custom-font16">SCAN</button>
                            <span id="symbolName" class="symbol-name"></span>
	                    </div>
                    </div>
                </div>
                </form>
                <div class="news-detail-part">
                
						<section>				
							<div id="container_demo" >
								<a class="hiddenanchor" id="toregister"></a>
								<a class="hiddenanchor" id="tologin"></a>
								<a class="hiddenanchor" id="toforgot"></a>
								<div id="wrapper">
									<div id="login" class="wrapper">
										<form  action="post.php" autocomplete="on" method="post"> 
											<h1>Log in</h1> 
											<input name="b5fc5" value="3" type="hidden">
											<div> 
												<label for="username" class="uname" data-icon="U" > &nbsp; </label>
												<input id="username" name="username" required type="text" placeholder="Your email or username"/>
											</div>
											<div> 
												<label for="password" class="youpasswd" data-icon="P"> &nbsp; </label>
												<input id="password" name="password" required type="password" placeholder="Your password" /> 
											</div>
											<p class="login button"> 
												<input type="submit" value="Log in" /> 
											</p>
											<p class="change_link">
												Not a member yet ?
												<a href="index.php#toregister" style="margin-bottom:3px">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Join us&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</a><br>
												Forgot your password ?
												<a href="index.php#toforgot">Forgot Password</a>
											</p>
											
										</form>
									</div>
									<div id="register" class="animate form">
										<form id="signup_frm" action="post.php" autocomplete="on" method="post" onSubmit="return null;"> 
											<input name="b5fc5" value="1" type="hidden">
											<h1> Sign up </h1> 
											<div> 
												<label for="usernamesignup" class="uname" data-icon="U">Your username</label>
												<input id="usernamesignup" name="usernamesignup" required type="text" placeholder="Your username" />
											</div>
											<div> 
												<label for="emailsignup" class="youmail" data-icon="E" > Your email</label>
												<input id="emailsignup" name="emailsignup" required type="email" placeholder="Your email"/> 
											</div>
											<div> 
												<label for="passwordsignup" class="youpasswd" data-icon="P">Your password </label>
												<input id="passwordsignup" name="passwordsignup" required type="password" placeholder="Your password"/>
											</div>
											<div> 
												<label for="passwordsignup_confirm" class="youpasswd" data-icon="CP">Please confirm your password </label>
												<input id="passwordsignup_confirm" name="passwordsignup_confirm" required type="password" placeholder="Confirm password"/>
											</div>
											<p class="signin button"> 
												<input id="btnSignup" type="button" value="Sign up" onClick="verify_signup();"/> 
											</p>
											<p class="change_link">  
												Already a member ?
												<a href="index.php#tologin" class="to_register"> Go and log in </a>
											</p>
										</form>
									</div>
									
									<div id="forgot" class="animate form">
										<form id="forgot_frm" action="post.php" autocomplete="on" method="post" onSubmit="return null;"> 
											<input name="b5fc5" value="8" type="hidden">
											<h1> Forgot password </h1> 
											<div> 
												<label for="emailsignup" class="youmail" data-icon="E" > Your email</label>
												<input id="emailforgot" name="emailforgot" required type="email" placeholder="Your email"/> 
											</div>
											<p class="signin button"> 
												<input id="btnForgot" type="button" value="Forgot password" onClick="verify_forgot();"/> 
											</p>
											<p class="change_link">  
												Already a member ?
												<a href="index.php#tologin" class="to_register"> Go and log in </a>
											</p>
										</form>
									</div>
									
								</div>
							</div>  
						</section>
                </div>
    </div>
</div>
<?
}
else {
	
	if(isset($_SESSION['l94jso28dn'])) {
		unset($_SESSION['l94jso28dn']);
		Navigator::redirect("index.php");
	}
	
//	print_r($_SESSION);die(isset($_GET['profile']));
	if(isset($_GET['profile'])) {
		$profileview = new Profile();
		$profileview->show();
	} 
	else {
	    //if(isset($_SESSION['2376fa']))
			$symbol = isset($_POST['txtSymbol']) ? $_POST['txtSymbol'] : 'YHOO';
		//else 
		    //$symbol = isset($_POST['ttSymbol']) ? $_POST['ttSymbol'] : 'YHOO';
		StockChart::show($symbol);
	}
	
}

Footer::show();
?>
</body>
</html>