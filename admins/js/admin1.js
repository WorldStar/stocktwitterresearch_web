// JavaScript Document

$(function () {
	$('#allcheck').click(function() {
		var checkboxes = $('#userTable').find(':checkbox');
		//alert(checkboxes);
		if($(this).is(':checked')) {
			checkboxes.prop('checked', 'checked');
		} else {
			checkboxes.prop('checked', '');
		}
	});
	
	$('#btnDeleteUser').click(function() {
		var values = $('input:checkbox:checked.tf').map(function () { // get selected checkbox values as array
		  return this.value;
		}).get();
		
		var unames = '';
		var uids = '';
		for(var i = 0; i < values.length; i ++) {
			if(values[i] == 'on')
				continue;
				
			unames += $('#uname'+values[i]).text();
			uids += values[i] + ',';
			
			if(i < values.length - 1)
				unames += ', ';
				
		}
		if(uids != ''){
			var res = confirm("Do you want to delete the following users? \n\r\n\r  " + unames);
			
			if(res) {
				$('#req').val('delete');
				$('#seled_uids').val(uids);
				$('#user_mng_frm').submit();
			}
		}
	});
	
	$('#btnSendMail').click(function() {
		var values = $('input:checkbox:checked.tf').map(function () { // get selected checkbox values as array
		  return this.value;
		}).get();
		
		var unames = '';
		var uids = '';
		for(var i = 0; i < values.length; i ++) {
			if(values[i] == 'on')
				continue;
				
			unames += $('#uname'+values[i]).text();
			uids += values[i] + ',';
			
			if(i < values.length - 1)
				unames += ', ';
				
		}
		if(uids != ''){
			var res = confirm("Do you want to email the following users? \n\r\n\r  " + unames);
			
			if(res) {
				$('#req').val('sendmail');
				$('#seled_uids').val(uids);
				$('#user_mng_frm').submit();
			}
		}
	});
	$('#lnkChangePass').click(function () {
		window.location = "index.php?34b9c=2";
	});
	$('#lnkUsrManage').click(function () {
		window.location = "index.php?34b9c=1&page=1";
	});
	$('#lnkSignout').click(function () {
		window.location = "post.php?34b9c=3";
	});
});
