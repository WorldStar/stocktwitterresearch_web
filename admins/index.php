<?php
session_start();
?>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>Admin Pannel</title>
<link rel="stylesheet" href="css/bootstrap.css">
<link rel="stylesheet" href="css/bootstrap-responsive.css">
<link rel="stylesheet" type="text/css" href="css/jquery.jqplot.css" />
<link rel="stylesheet" type="text/css" href="syntaxhighlighter/styles/shCoreDefault.css" />
<link rel="stylesheet" type="text/css" href="syntaxhighlighter/styles/shThemejqPlot.css" />
<link href="css/bootstrap-combined.no-icons.css" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="css/style.css">
<link rel="stylesheet" href="css/table.css" type="text/css"/>	
<!-- Login -->
<link rel="stylesheet" type="text/css" href="../css/login/style.css" />
<link rel="stylesheet" type="text/css" href="../css/login/animate-custom.css" />
<!----------->

<script src="js/jquery.js"></script>
<script src="js/admin.js"></script>

</head>
<?php
include_once("../controller/exhandler.php");
include_once("../controller/usermng.php");
include_once("../conf/definition.php");
include_once("../model/mysql.lib.php");
include_once("../view/home.php");
include_once("../encrypt/security_module.php");
 include_once("../encrypt/config.php");
 include_once("../encrypt/encrpt.php");
 include_once("../encrypt/decrpt.php");
 include_once("../encrypt/encrpt2.php");
 include_once("../encrypt/decrpt2.php");

?>
<body>
<?php
if(ExceptionHandler::neededToShowMsg())
	ExceptionHandler::showMsgDiag();

if($_SESSION['3d91n7s'] == '')
	Home::showAdmin();
else {
?>
		<!--Header--> 
           <div class="row-fluid header-container">
            <div style="float:left; margin-left:40px;">
                <a href="'.SITE_URL.'" target="_self" title="tdameritrade"><img src="../img/logo.png"/></a>
            </div>  
            <div style="float:right; margin-right:50px;">  
                <a href="javascript:;" title="" id="lnkUsrManage">User Management</a>
                <a href="javascript:;" title="" id="lnkChangePass">Change pass</a>
                <a href="javascript:;" title="" id="lnkSignout">Sign out</a>
            </div>  
           </div>
		<!--Header--> 
				<div class="container-fluid" style="text-align:center; padding: 20px;">
<?php
$usr = new UserManager();
switch( $_REQUEST['34b9c'] ) {
	default:
	case 1:
		
		$page = isset($_REQUEST['page']) ? $_REQUEST['page'] : 1;
		
		$usr->listUsers($page, 20);
		
		break;
	case 2:
		Home::showChangePassForm();
		
		break;
	case 3:
		Home::showMessageForm();
		
		break;
}
?>
				</div>
<?php
}
?>
</body>
</html>
