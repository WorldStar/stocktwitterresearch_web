<?php
define(SITE_URL, 'http://www.twitterstockresearch.com');
define( 'WEB_ROOT', $_SERVER['DOCUMENT_ROOT']."" );
define(BASIC_URL,"http://".$_SERVER['HTTP_HOST']."");
define(FAILED, 10000);
define(SUCCESS, 10001);

// MySQL
define(MY_HOST, "www.twitterstockresearch.com");
define(MY_USER, "twitters_user");
define(MY_PASS, "4Y2rnqr9J4");
define(MY_DB, "twitters_DB");

// registration email
define(REG_MAIL_TITLE, "Welcome to StockTwits!");

define(REG_MAIL_CONTENTS, SITE_URL."/post.php?b5fc5=2");

// msgs
define(SENT_CONFIRM_MAIL, "An email has been sent to your email address to verify. <br>Please verify your email address and enjoy!");
define(SENT_CONFIRM_MAIL_FAILED, "Your mail address can't be confirmed. <br>Try again with aother email address.");
define(EMAIL_CHANGED, "Your email has been changed. <br>An email has been sent to your new email address to verify. <br>Please verify your email address and enjoy!");
define(DUPLICATED_USERNAME, "The user name is already in use. <br>Please try with other user name.");
define(DUPLICATED_EMAIL, "The email is already in use. <br>Please try with other email.");
define(UNCONFIRMED_EMAIL, "The email is not verified yet. <br>Please check your mail box and verify it by clicking the link.");
define(WRONG_EMAIL, "User name or email is wrong. <br>Pleae try again. <br>If you didn't verify your email yet, please kindly verify it now.");
define(WRONG_PASSWORD, "Password is wrong.<br>Pleae try again.");
define(EMAIL_CONFIRM_SUCCESS, "Your email address has been confirmed.<br>Please log in and edit your profile to fill up all infomation needed for service.");
define(EMAIL_CONFIRM_FAILED, "The link was expired.<br>Try to log in now.");
?>