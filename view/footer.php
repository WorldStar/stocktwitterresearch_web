<?php
class Footer {
	public function show() {
		echo '
				   <div class="row-fluid footer-container">
					<div>
						<a href="https://www.tdameritrade.com/home.page" target="_blank" title="tdameritrade"><img src="img/logo1.png"/></a>
						<a href="https://www.fidelity.com/" target="_blank" title="fidelity"><img src="img/logo2.png"/></a>
						<a href="https://us.etrade.com/home" target="_blank" title="etrade"><img src="img/logo3.png"/></a>
						<a href="https://www.scottrade.com/" target="_blank" title="scottrade"><img src="img/logo4.png"/></a>
						<a href="https://www.schwab.com/" target="_blank" title="schwab"><img src="img/logo5.png"/></a>
						<a href="http://www.optionsxpress.com/" target="_blank" title="optionsxpress"><img src="img/logo6.png"/></a>
					</div>  
				   </div>
			';
	}
}
?>