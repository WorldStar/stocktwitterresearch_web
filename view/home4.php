<?php
class Home {
	public function show() {
		echo '
			<div class="container back-white">
				<div class="container-fluid" style="text-align:center">
						<div>
						<!-- main content part -->
							<form class="form-search" method="post" action="index.php">
							<div class="info-part">
								<div class="subcontents">
									<div class="yui3-skin-sam" style="padding-top:50px"><!--DIV FOR AUTOCOMPLETE-->
										<input type="text" id="txtSymbol" class="input-xlarge" value="" name="txtSymbol" value="Symbol" autocomplete="off" placeholder="(Stock Symbol)">
										<input type="submit" class="btn btn-info custom-font16" value="Research" name="btnSymbol" />
										<!--<span id="symbolName" class="symbol-name"> Yahoo! Inc. </span>-->
								  </div>
								</div>
							</div>
							</form>
						</div>
						<div style="height:30px"></div>
						<!--------------------------------------------------------------->
						<!-- Login Form -->
						<section>				
							<div id="container_demo" >
								<a class="hiddenanchor" id="toregister"></a>
								<a class="hiddenanchor" id="tologin"></a>
								<div id="wrapper">
									<div id="login" class="animate form">
										<form  action="post.php" autocomplete="on" method="post"> 
											<h1>Log in</h1> 
											<input name="b5fc5" value="3" type="hidden">
											<div> 
												<label for="username" class="uname" data-icon="U" > &nbsp; </label>
												<input id="username" name="username" required type="text" placeholder="Your email or username"/>
											</div>
											<div> 
												<label for="password" class="youpasswd" data-icon="P"> &nbsp; </label>
												<input id="password" name="password" required type="password" placeholder="Your password" /> 
											</div>
											<p class="login button"> 
												<input type="submit" value="Login" /> 
											</p>
											<p class="change_link">
												Not a member yet ?
												<a href="index.php#toregister">Join us</a>
											</p>
										</form>
									</div>
			
									<div id="register" class="animate form">
										<form id="signup_frm" action="post.php" autocomplete="on" method="post" onSubmit="return null;"> 
											<input name="b5fc5" value="1" type="hidden">
											<h1> Sign up </h1> 
											<div> 
												<label for="usernamesignup" class="uname" data-icon="U">Your username</label>
												<input id="usernamesignup" name="usernamesignup" required type="text" placeholder="Your username" />
											</div>
											<div> 
												<label for="emailsignup" class="youmail" data-icon="E" > Your email</label>
												<input id="emailsignup" name="emailsignup" required type="email" placeholder="Your email"/> 
											</div>
											<div> 
												<label for="passwordsignup" class="youpasswd" data-icon="P">Your password </label>
												<input id="passwordsignup" name="passwordsignup" required type="password" placeholder="Your password"/>
											</div>
											<div> 
												<label for="passwordsignup_confirm" class="youpasswd" data-icon="CP">Please confirm your password </label>
												<input id="passwordsignup_confirm" name="passwordsignup_confirm" required type="password" placeholder="Confirm password"/>
											</div>
											<p class="signin button"> 
												<input id="btnSignup" type="button" value="Sign up" onClick="verify_signup();"/> 
											</p>
											<p class="change_link">  
												Already a member ?
												<a href="index.php#tologin" class="to_register"> Go and log in </a>
											</p>
										</form>
									</div>
									
								</div>
							</div>  
						</section>
					</div>
				</div>
		';
	}
	public function showAdmin() {
		echo '
				<div class="container-fluid" style="text-align:center">
						<div style="height:30px"></div>
						<div>
						<section>				
							<div id="container_demo" >
								<a class="hiddenanchor" id="toregister"></a>
								<a class="hiddenanchor" id="tologin"></a>
								<div id="wrapper">
									<div id="login" class="animate form">
										<form  action="post.php" autocomplete="on" method="post"> 
											<h1>Log in as administrator</h1> 
											<input name="34b9c" value="2" type="hidden">
											<div> 
												<label for="username" class="uname" data-icon="U" > Admin name </label>
												<input id="username" name="username" required type="text" placeholder="Username"/>
											</div>
											<div> 
												<label for="password" class="youpasswd" data-icon="P"> Admin password </label>
												<input id="password" name="password" required type="password" placeholder="Password"/> 
											</div>
											<p class="login button"> 
												<input type="submit" value="Login" '.($_SESSION['8ab9']<3 ? '' : 'disabled').'/> 
											</p>
										</form>
									</div>
							</div>
						</section>
					</div>
				</div>
		';
	}
	
	public function showChangePassForm() {
		echo '
				<div class="container-fluid" style="text-align:center">
						<div style="height:30px"></div>
						<!--------------------------------------------------------------->
						<!-- Login Form -->
						<section>				
							<div id="container_demo" >
								<a class="hiddenanchor" id="toregister"></a>
								<a class="hiddenanchor" id="tologin"></a>
								<div id="wrapper">
									<div id="login" class="animate form">
										<form  id="changepass_frm" action="post.php" autocomplete="on" method="post" onSubmit="return null;"> 
											<input name="34b9c" value="4" type="hidden">
											<h1> Change password </h1> 
											<div> 
												<label for="passwordsignup" class="youpasswd" data-icon="P">Your password </label>
												<input id="passwordsignup" name="passwordsignup" required type="password" placeholder="Your password"/>
											</div>
											<div> 
												<label for="passwordsignup_confirm" class="youpasswd" data-icon="CP">Please confirm your password </label>
												<input id="passwordsignup_confirm" name="passwordsignup_confirm" required type="password" placeholder="Confirm password"/>
											</div>
											<p class="signin button"> 
												<input id="btnSignup" type="button" type="button" value="Save" onClick="verify_changepass();"/> 
												<input type="button" value="Cancel" onClick="history.back()">
											</p>
										</form>
									</div>
									
								</div>
							</div>  
						</section>
					</div>
				</div>
		';
	}
	public function showMessageForm() {
		echo '
				<div class="container-fluid" style="text-align:center">
						<div style="height:30px"></div>
						<!--------------------------------------------------------------->
						<!-- Login Form -->
						<section>				
							<div id="container_demo" >
								<a class="hiddenanchor" id="toregister"></a>
								<a class="hiddenanchor" id="tologin"></a>
								<div id="wrapper">
									<div id="login" class="animate form">
										<form  action="post.php" autocomplete="on" method="post"> 
											<input name="34b9c" value="5" type="hidden">
											<h1> Send Mail </h1> 
											<div> 
												<label for="subject" class="uname">Mail subject </label>
												<input id="subject" name="subject" required type="text" placeholder="Subject"/>
											</div>
											<div> 
												<label for="message" class="uname">Mail content </label>
												<textarea id="message" name="message" rows="10" required="" placeholder="      Content" style="margin-left: 0px; margin-right: 0px; width: 423px;"></textarea>						
											</div>
											<p class="signin button"> 
												<input type="submit" value="SendMail"/> 
												<input type="button" value="Cancel" onClick="history.back()">
											</p>
										</form>
									</div>
									
								</div>
							</div>  
						</section>
					</div>
				</div>
		';
	}
	
}
?>