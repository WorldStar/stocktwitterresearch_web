<?php
include_once("./controller/usermng.php");
include_once("./encrypt/security_module.php");

class Profile {
	protected $usermng;
	protected $uid;
	protected $info;
	protected $fname;
	protected $uname;
	protected $email;
	protected $email2;
	protected $addr;
	
	public function __construct() {
		$this->usermng = new UserManager();
		$this->info = $this->usermng->getProfile($this->uid = $_SESSION['2376fa']);
		if($this->info['fname'] != null && $this->info['fname'] != ""){
			$this->fname = $this->usermng->convert->myDecrypt($this->info['fname']);
		}else{
		   $this->fname = '';
		}
		if($this->info['uname'] != null && $this->info['uname'] != ""){
			$this->uname = $this->usermng->convert->myDecrypt($this->info['uname']);
		}else{
		   $this->uname = '';
		}
		if($this->info['email'] != null && $this->info['email'] != ""){
			$this->email = $this->usermng->convert->myDecrypt($this->info['email']);
		}else{
		   $this->email = '';
		}
		if($this->info['email_2'] != null && $this->info['email_2'] != ""){
			$this->email2 = $this->usermng->convert->myDecrypt($this->info['email_2']);
		}else{
		   $this->email2 = '';
		}
		if($this->info['addr'] != null && $this->info['addr'] != ""){
			$this->addr = $this->usermng->convert->myDecrypt($this->info['addr']);
		}else{
		   $this->addr = '';
		}
	}
	
	public function show() {
		echo '
			<div class="container back-white">
				<div class="container-fluid" style="text-align:center">
						<div style="height:20px"></div>
						<!--------------------------------------------------------------->
						<!-- Login Form -->
						<section>				
							<div id="container_demo" >
								<a class="hiddenanchor" id="toregister"></a>
								<a class="hiddenanchor" id="tologin"></a>
								<div id="wrapper">
									<div id="login" class="animate form">
										<form  action="post.php" autocomplete="on" method="post"> 
											<input name="b5fc5" value="5" type="hidden">
											<input name="uid" value="'.$this->uid.'" type="hidden">
											<h1> Profile </h1> 
											<div> 
												<label for="fullname" class="uname" data-icon="F">Your full name</label>
												<input id="fullname" name="fullname" required type="text" value="'.$this->fname.'" placeholder="e.g. John Miller" />
											</div>
											<div> 
												<label for="username" class="uname" data-icon="U">Your user name</label>
												<input id="username" name="username" required readonly type="text" value="'.$this->uname.'" />
											</div>
											<div> 
												<label for="email" class="youmail" data-icon="E" > Your email</label>
												<input id="email" name="email" required type="email" value="'.$this->email.'"/> 
											</div>
											<div> 
												<label for="email2" class="youmail" data-icon="E" > Your secondary email</label>
												<input id="email2" name="email2" type="email" value="'.$this->email2.'" placeholder="Needed for your password recovery"/> 
											</div>
<!--											<div> 
												<label for="addr" class="uname" data-icon="A" > Your address</label>
												<input id="addr" name="addr" required type="text" value="'.$this->addr.'" placeholder="e.g. John Miller"/> 
											</div>
-->											<p class="signin button"> 
												<input type="submit" value="Save"/> 
												<input type="button" value="Back" onClick="history.back();"/> 
											</p>
										</form>
									</div>
								
								</div>
							</div>  
						</section>
<!--						<div style="height:30px"></div>
-->					</div>
				</div>
		';
	}
}
?>