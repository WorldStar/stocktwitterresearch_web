<?php
session_start();

class ExceptionHandler {
	
	public function getErrMsg(){
		return $_SESSION['err_msg'];
	}
	
	public function showMsgDiag() {
		echo '
			<script src="http://code.jquery.com/jquery-1.9.1.js"></script>
			<script src="http://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>
			
			<script>
			$(function() {
			$( "#dialog" ).dialog();
			});
			</script>
			
			<div id="dialog" title="Note: StockTwits ">
			<p>'.$_SESSION['err_msg'].'</p>
			</div>
		';
		
		$_SESSION['neededToShowError'] = false;
	}
	
	public function needToShowErrMsg($msg) {
		$_SESSION['err_msg'] = $msg;
		
		$_SESSION['neededToShowError'] = true;
		
	}
	
	public function neededToShowMsg() {
		return $_SESSION['neededToShowError'];
	}
}
?>
