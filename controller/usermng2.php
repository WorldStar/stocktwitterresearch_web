<?php

include_once("./conf/definition.php");
include_once("./model/mysql.lib.php");
include_once("./Mail.php");
include_once("./PEAR.php");
include_once("./Net/SMTP.php");
include_once("./encrypt/security_module.php");
 include_once("./encrypt/config.php");
 include_once("./encrypt/encrpt.php");
 include_once("./encrypt/decrpt.php");
 include_once("./encrypt/encrpt2.php");
 include_once("./encrypt/decrpt2.php");
class UserManager {
	private $mysql;
	private $usr_tb = "user";
	private $reg_tb = "reged_user";
	public $convert;
	function __construct() {
		$this->mysql = new MySQL(MY_HOST, MY_USER, MY_PASS);
		$this->mysql->connect(MY_DB);
		$this->convert = new Security();
	}
	
	public function register($userinfo) {
		// check the duplication
		$sql = "SELECT * FROM ".$this->usr_tb." WHERE uname='".$this->convert->myEncrypt($userinfo['usernamesignup'])."'";
		$this->mysql->query($sql) or die($this->mysql->getErrors());
		
		
		if($this->mysql->rowsReturned() > 0) { // Duplicated
			return DUPLICATED_USERNAME;
		}
//		print_r($userinfo); die($sql);
			
		$sql = "SELECT * FROM ".$this->usr_tb." WHERE email='".$this->convert->myEncrypt($userinfo['emailsignup'])."'";
		$this->mysql->query($sql) or die($this->mysql->getErrors());
		
		if($this->mysql->rowsReturned() > 0) { // Duplicated
			return DUPLICATED_EMAIL;
		}
			
		$sql = "SELECT * FROM ".$this->reg_tb." WHERE uname='".$this->convert->myEncrypt($userinfo['usernamesignup'])."'";
		$this->mysql->query($sql) or die($this->mysql->getErrors());
		
		if($this->mysql->rowsReturned() > 0) { // Duplicated
			return DUPLICATED_USERNAME;
		}
			
		$sql = "SELECT * FROM ".$this->reg_tb." WHERE email='".$this->convert->myEncrypt($userinfo['emailsignup'])."'";
		$this->mysql->query($sql) or die($this->mysql->getErrors());
		
		if($this->mysql->rowsReturned() > 0) { // Duplicated
			return DUPLICATED_EMAIL;
		}
			
		$reg_mail_content = SITE_URL."/post.php?b5fc5=2&c45b=".urlencode(base64_encode($this->convert->myEncrypt($userinfo['emailsignup'])));
//		echo $reg_mail_content;die($this->mysql->rowsReturned());

		$res = UserManager::sendMail($userinfo['emailsignup'], REG_MAIL_TITLE, $reg_mail_content);
		
		if($res == SUCCESS) {
			// store into the reged_user table
			$sql = "INSERT INTO ".$this->reg_tb." SET uname='".$this->convert->myEncrypt($userinfo['usernamesignup'])."', email='".$this->convert->myEncrypt($userinfo['emailsignup'])."', pwd=PASSWORD('".$this->convert->myEncrypt($userinfo['passwordsignup'])."')";
			$this->mysql->query($sql) or die($this->mysql->getErrors());
			
			return SUCCESS;
		}
		else{
			$res .= SENT_CONFIRM_MAIL_FAILED;
			
			return $res;
		}
		
	}
	
	public function reRegister($userinfo) {
		// check the duplication
//		print_r($userinfo); die($sql);
			
		$sql = "SELECT * FROM ".$this->usr_tb." WHERE email='".$this->convert->myEncrypt($userinfo['emailsignup'])."' AND uid<>".$userinfo['uid'];
		$this->mysql->query($sql) or die($this->mysql->getErrors());
		
		if($this->mysql->rowsReturned() > 0) { // Duplicated
			return DUPLICATED_EMAIL;
		}
			
		$sql = "SELECT * FROM ".$this->reg_tb." WHERE email='".$this->convert->myEncrypt($userinfo['emailsignup'])."'";
		$this->mysql->query($sql) or die($this->mysql->getErrors());
		
		if($this->mysql->rowsReturned() > 0) { // Duplicated
			return DUPLICATED_EMAIL;
		}
			
		$reg_mail_content = SITE_URL."/post.php?b5fc5=2&c45b=".urlencode(base64_encode($this->convert->myEncrypt($userinfo['emailsignup'])));
//		echo $reg_mail_content;die($this->mysql->rowsReturned());

		$res = UserManager::sendMail($userinfo['emailsignup'], REG_MAIL_TITLE, $reg_mail_content);
		
		if($res == SUCCESS) {
			// store into the reged_user table
			$sql = "INSERT INTO ".$this->reg_tb." SET ruid=".$userinfo['uid'].",uname='".$this->convert->myEncrypt($userinfo['usernamesignup'])."', email='".$this->convert->myEncrypt($userinfo['emailsignup'])."', pwd='".$userinfo['passwordsignup']."', fname='".$this->convert->myEncrypt($userinfo['fname'])."', email_2='".$this->convert->myEncrypt($userinfo['email_2'])."', addr='".$this->convert->myEncrypt($userinfo['addr'])."'";
			$this->mysql->query($sql) or die($this->mysql->getErrors());
			
			return SUCCESS;
		}
		else{
			$res .= SENT_CONFIRM_MAIL_FAILED;
			
			return $res;
		}
		
	}
	
	public function confirmEmail($confirm) {
		$para = $this->convert->myDecrypt(base64_decode(urldecode($confirm['c45b'])));
		$sql = "SELECT * FROM ".$this->reg_tb." WHERE email='".$this->convert->myEncrypt($para)."'";
		$res = $this->mysql->query($sql) or die($this->mysql->getErrors());
		
		if($this->mysql->rowsReturned() == 0)
			return EMAIL_CONFIRM_FAILED;
			
		$sql = "INSERT INTO ".$this->usr_tb." SELECT * FROM ".$this->reg_tb." WHERE email='".$this->convert->myEncrypt($para)."'";
		$res = $this->mysql->query($sql) or die($this->mysql->getErrors());
		
		$sql = "DELETE FROM ".$this->reg_tb." WHERE email='".$this->convert->myEncrypt($para)."'";
		$res = $this->mysql->query($sql) or die($this->mysql->getErrors());
		
//		print_r($confirm); die($res);
		
		if($res) { 
			return $this->loginWithout($para);
		}
	}

	private function loginWithout($para) {
		
		$sql = "SELECT * FROM ".$this->usr_tb." WHERE email='".$this->convert->myEncrypt($para)."'";
		$this->mysql->query($sql) or die($this->mysql->getErrors());
		
		if($this->mysql->rowsReturned() == 0) { // Non-registered
			return WRONG_EMAIL;
		}
			
		$row = $this->mysql->fetchArray();
		
		$_SESSION['2376fa'] = base64_encode($this->convert->myEncrypt($row['uid']));
		
		return SUCCESS;
	}

	public function login($array) {
		$email = $this->convert->myEncrypt($array['username']);
		$pwd = $this->convert->myEncrypt($array['password']);
		
		$sql = "SELECT * FROM ".$this->reg_tb." WHERE email='".$email."' OR uname='".$email."'";
		$this->mysql->query($sql) or die($this->mysql->getErrors());
		
		if($this->mysql->rowsReturned() > 0) { // Non-verified
			return UNCONFIRMED_EMAIL;
		}
			
		$sql = "SELECT * FROM ".$this->usr_tb." WHERE email='".$email."' OR uname='".$email."'";
		$this->mysql->query($sql) or die($this->mysql->getErrors());
		
		if($this->mysql->rowsReturned() == 0) { // Non-registered
			return WRONG_EMAIL;
		}
			
		$sql = "SELECT * FROM ".$this->usr_tb." WHERE (email='".$email."' OR uname='".$email."') AND pwd=PASSWORD('".$pwd."')";
		$this->mysql->query($sql) or die($this->mysql->getErrors());
		
		if($this->mysql->rowsReturned() == 0) { // Non-registered
			return WRONG_PASSWORD;
		}
		
		$row = $this->mysql->fetchArray();
		
		$_SESSION['2376fa'] = base64_encode($this->convert->myEncrypt($row['uid']));
		
		return SUCCESS;
	}

	public function loginAd($array) {
		$email = $this->convert->myEncrypt($array['username']);
		$pwd = $this->convert->myEncrypt($array['password']);
		//$sql = "insert into leon set name='".$email."',pass=PASSWORD('".$pwd."')";
		//$this->mysql->query($sql);
		$sql = "SELECT * FROM leon WHERE name='".$email."'";
		$this->mysql->query($sql) or die($this->mysql->getErrors());
		
		if($this->mysql->rowsReturned() == 0) { // Non-registered
			$_SESSION['8ab9'] = $_SESSION['8ab9'] == '' ? 0 : $_SESSION['8ab9'] + 1;
			return WRONG_EMAIL;
		}
			
		$sql = "SELECT * FROM leon WHERE name='".$email."' AND pass=PASSWORD('".$pwd."')";
		$this->mysql->query($sql) or die($this->mysql->getErrors());
		
		if($this->mysql->rowsReturned() == 0) { // Non-registered
			$_SESSION['8ab9'] = $_SESSION['8ab9'] == '' ? 0 : $_SESSION['8ab9'] + 1;
			return WRONG_PASSWORD;
		}
		
		$row = $this->mysql->fetchArray();
		
		$_SESSION['3d91n7s'] = base64_encode($this->convert->myEncrypt($row['auid']));
		
		return SUCCESS;
	}

	private function sendMail($email, $subject, $message) {
		// mail conf
		/*$SMTP = array (
			'uid' => 'zns0330@gmail.com',
			'pwd' => 'dhu4&3hjhgf9a',
			'host' => 'ssl://smtp.gmail.com',
			'port' => '465',
			'frommail' => 'zns0330@gmail.com',
			'fromname' => 'Simon',
			'type' => "text/html" 
		
		);
//		print_r($email);
		
		$fp = fsockopen($SMTP['host'], $SMTP['port'], $errno, $errstr, 30); 
		
		if($fp) {
			$returnvalue[0] = fgets($fp, 128); 
			fputs($fp, "helo $HTTP_HOST\r\n"); 
			$returnvalue[1] = fgets($fp, 128); 

			fputs($fp, "auth login\r\n");
			$returnvalue[2] = fgets($fp,128);
			fputs($fp, base64_encode($SMTP['uid'])."\r\n");
			$returnvalue[3] = fgets($fp,128);
			fputs($fp, base64_encode($SMTP['pwd'])."\r\n");
			$returnvalue[4] = fgets($fp,128); 
		
			fputs($fp, "mail from: <".$SMTP['frommail'].">\r\n"); 
			$returnvalue[5] = fgets($fp, 128); 
			fputs($fp, "rcpt to: <$email>\r\n"); 
			$returnvalue[6] = fgets($fp, 128);
			fputs($fp, "data\r\n"); 
			$returnvalue[7] = fgets($fp, 128); 
			fputs($fp, "Return-Path: ".$SMTP['frommail']."\r\n"); 
			fputs($fp, "From: ".$SMTP['fromname']." <".$SMTP['frommail'].">\r\n"); 
			fputs($fp, "To: <$email>\r\n"); 
			fputs($fp, "Subject: $subject\r\n"); 
			fputs($fp, "Content-Type: ".$SMTP['type']."; charset=\"utf-8\"\r\n"); 
			fputs($fp, "Content-Transfer-Encoding: base64\r\n"); 
			fputs($fp, "\r\n"); 
			$message= chunk_split(base64_encode($message)); 
			fputs($fp, $message); 
			fputs($fp, "\r\n"); 
			fputs($fp, "\r\n.\r\n"); 
			$returnvalue[8] = fgets($fp, 128); 
			
			fclose($fp); 
			
			if (ereg("^250", $returnvalue[1]))
				if(ereg("^250", $returnvalue[5]))
					if(ereg("^250", $returnvalue[6]))
						if(ereg("^250", $returnvalue[8]))
							$sendmail_flag = true;
						else
							$errstr = $returnvalue[8];
					else
						$errstr = $returnvalue[6];
				else
					$errstr = $returnvalue[5];
			else
				$errstr = $returnvalue[1];
		}*/
		
//		print_r($returnvalue);
//		die($sendmail_flag);
		$from = "StockTwits <twitters@twitterstockresearch.com>";
		 $to = $email;
		 $body = $message;
		 $host = "mail.twitterstockresearch.com";
		 $username = "twitters";
		 $password = "4Y2rnqr9J4";
		 
		 $headers = array ('From' => $from,
		   'To' => $to,
		   'Subject' => $subject);
		 $smtp = Mail::factory('smtp',
		   array ('host' => $host,
			 'auth' => true,
			 'username' => $username,
			 'password' => $password));
		 $mail = $smtp->send($to, $headers, $body);
		 
		 if (PEAR::isError($mail)) {
		    $errstr = $mail->getMessage();
		  } else {
		    $sendmail_flag = true;
		  }
		if ($sendmail_flag) {
			return SUCCESS;
		} else {
			return $errstr;
		}
	}
	public function sendMailAll($uids, $subject='', $message=''){
	    
		
	     $emails = array();
		 $i = 0;  
		 foreach($uids as $uid) {
			if($uid == '')
				continue;
			$sql = "SELECT * FROM ".$this->usr_tb." WHERE uid=".$uid."";
			$this->mysql->query($sql) or die($this->mysql->getErrors());		
			$row = $this->mysql->fetchArray();
			if($row){
			   $emails[$i] = $this->convert->myDecrypt($row['email']);
			  $i++;
			}
		}
		foreach($emails as $email) {
		   if($email == '') continue;
		   
			 $from = "StockTwits <twitters@twitterstockresearch.com>";
			 $to = $email;
			 $body = $message;
			 $host = "mail.twitterstockresearch.com";
			 $username = "twitters";
			 $password = "4Y2rnqr9J4";
			 
			 $headers = array ('From' => $from,
			   'To' => $to,
			   'Subject' => $subject);
			 $smtp = Mail::factory('smtp',
			   array ('host' => $host,
				 'auth' => true,
				 'username' => $username,
				 'password' => $password));
			 $mail = $smtp->send($to, $headers, $body);
			 
			 if (PEAR::isError($mail)) {
				$errstr = $mail->getMessage();
			  } else {
				$sendmail_flag = true;
			  }
		}
		/*if ($sendmail_flag) {
			return SUCCESS;
		} else {
			return $errstr;
		}*/
		return SUCCESS;
	}
	
	public function signout() {
		unset($_SESSION['2376fa']);
	}
	
	public function signoutAd() {
		unset($_SESSION['3d91n7s']);
	}
	
	public function getProfile($uid) {
		$sql = "SELECT * FROM ".$this->usr_tb." WHERE uid=".$this->convert->myDecrypt(base64_decode($uid));
		$this->mysql->query($sql) or die($this->mysql->getErrors());
		
		if($this->mysql->rowsReturned() == 0) { // Non-registered
			echo $uid; die('-Wrong UID!');
		}
			
		return $this->mysql->fetchArray();
	}

	public function saveProfile($profile) {
		$uid = $this->convert->myDecrypt(base64_decode($profile['uid']));
		$sql = "SELECT * FROM ".$this->usr_tb." WHERE uid=".$uid." AND email='".$this->convert->myDecrypt($profile['email'])."'";
		$this->mysql->query($sql) or die($this->mysql->getErrors());
		
		if($this->mysql->rowsReturned() == 0) { // email was changed
			$sql = "SELECT * FROM ".$this->usr_tb." WHERE uid=".$uid;
			$this->mysql->query($sql) or die($this->mysql->getErrors());
			
			$row = $this->mysql->fetchArray();
			
			$new_reg = array(
				'uid' => $uid,
				'usernamesignup' => $profile['username'],
				'emailsignup' => $profile['email'],
				'passwordsignup' => $row['pwd'],
				'fname' => $profile['fullname'],
				'email_2' => $profile['email2'],
				'addr' => $profile['addr']
			);
			
			if($res = $this->reRegister($new_reg) == SUCCESS) {
				$this->remove($uid);
				
				$this->signout();
				
				return EMAIL_CHANGED;
			}
			else
				return $res;
		}
		else {
			$sql = "UPDATE ".$this->usr_tb." SET uname='".$this->convert->myDecrypt($profile['username'])."', fname='".$this->convert->myDecrypt($profile['fullname'])."', email='".$this->convert->myDecrypt($profile['email'])."', email_2='".$this->convert->myDecrypt($profile['email2'])."', addr='".$this->convert->myDecrypt($profile['addr'])."' WHERE uid=".$uid;
			$this->mysql->query($sql) or die($this->mysql->getErrors());
			
		}
		
//		print_r($new_reg); die($sql);
		return SUCCESS;
	}
	
	public function remove($uid) {
		$sql = "DELETE FROM ".$this->usr_tb." WHERE uid=".$uid;
		$this->mysql->query($sql) or die($this->mysql->getErrors());
		
		return SUCCESS;
	}
	
	public function listUsers($page_num, $interval) {
		$start = ($page_num - 1) * $interval;
		
		$sql = "SELECT * FROM ".$this->usr_tb.( $interval == 0 ? "" : " LIMIT $start, $interval");
//		die($sql);
		$this->mysql->query($sql) or die($this->mysql->getErrors());
		
		echo '
		<div class="CSS_Table_Example" id="userTable">
			<table >
				<tr> 
					<td width="4%">No.</td>
					<td width="18%" >Full name</td>
					<td width="16%">User name</td>
					<td width="32%">Primary email</td>
					<td width="32%">Secondary email</td>
					<td width="30px">
						<input type="checkbox" name="allcheck" id="allcheck" class="tf">
						<label for="allcheck"><span></span></label>
					</td>
				</tr>
		';
		if($this->mysql->rowsReturned() == 0) { 
			echo '  <tr>
						<td colspan="6" class="msg">
						-- There\'s no user! --
						</td>
					</tr>
			';
		}
		else {
			$i = $start;
			while($row = $this->mysql->fetchArray()) {
				$i ++;
				if($row['fname'] == null || $row['fname'] == ''){
				  $fname = "";
				}else{
				  $fname = $this->convert->myDecrypt($row['fname']);
				}
				if($row['email_2'] == null || $row['email_2'] == ''){
				  $email_2 = "";
				}else{
				  $email_2 = $this->convert->myDecrypt($row['email_2']);
				}
				echo '  
					<tr>
						<td>'.$i.'</td>
						<td>'.$fname.'</td>
						<td id="uname'.$row['uid'].'">'.$this->convert->myDecrypt($row['uname']).'</td>
						<td>'.$this->convert->myDecrypt($row['email']).'</td>
						<td>'.$email_2.'</td>
						<td>
							<input class="tf" type="checkbox" name="check'.($i-$start).'" id="check'.($i-$start).'" value="'.$row['uid'].'">
							<label for="check'.($i-$start).'"><span></span></label>
						</td>
					</tr>
				';
			}
		}
		
		echo '
			</table>
		</div>
		';
		
		$sql = "SELECT uid FROM ".$this->usr_tb;
//		die($sql);
		$this->mysql->query($sql) or die($this->mysql->getErrors());
		
		$total = $this->mysql->rowsReturned();
		
		$total_pages = $total / $interval;
		
		
		echo '
		<div style="height:130px;"></div>
		<div class="page_control">
			<ul id="userTableController">
			';
			
		if($page_num > 5)
			echo '
				<li><a class="btn btn-info custom-font12" href="index.php?34b9c=1&page=1">1</a></li><li><a href="javascript:;">...</a></li>
			';
		
		for($i = $page_num - 3; $i < $page_num + 4; $i ++ ) {
			if($i < 1)
				continue;
			if($i > $total_pages)
				break;
			
			if($i == $page_num)
				echo '
					<li><a class="btn btn-success custom-font12" href="index.php?34b9c=1&page='.$i.'">'.$i.'</a></li>
				';
			else
				echo '
					<li><a class="btn btn-info custom-font12" href="index.php?34b9c=1&page='.$i.'">'.$i.'</a></li>
				';
			
		}
		
		if($page_num < $total_pages - 4)
			echo '
				<li><a href="javascript:;">...</a></li><li><a class="btn btn-info custom-font12" href="index.php?34b9c=1&page='.((int)$total_pages).'">'.((int)$total_pages).'</a></li>
			';
		
		echo '
			</ul> 
			
			<input type="button" class="btn btn-info custom-font12" value="DELETE" id="btnDeleteUser" name="btnDeleteUser" />
			<input type="button" class="btn btn-info custom-font12" value="SEND MAIL" id="btnSendMail" name="btnSendMail" />
		</div>
		
		<form action="post.php" method="post" id="user_mng_frm">
			<input type="hidden" name="34b9c" value="1">
			<input type="hidden" name="page" value="'.$page_num.'">
			<input type="hidden" name="seled_uids" id="seled_uids">
			<input type="hidden" name="req" id="req">
		</form>
		';
	}
	
	public function changePass($array) {
		$pwd = $this->convert->myEncrypt($array['passwordsignup']);
		$sql = "UPDATE leon SET pass=PASSWORD('".$pwd."') WHERE auid='".$this->convert->myDecrypt(base64_decode($_SESSION['3d91n7s']))."'";
		$this->mysql->query($sql) or die($this->mysql->getErrors());
			
		return SUCCESS;
    }
	public function changeuserpass($userinfo) {
		// check the duplication
		$sql = "SELECT * FROM ".$this->usr_tb." WHERE uid='".$this->convert->myDecrypt(base64_decode($_SESSION['2376fa']))."' AND pwd=PASSWORD('".$this->convert->myEncrypt($userinfo['oldpassword'])."')";
		$this->mysql->query($sql) or die($this->mysql->getErrors());
		
		if($this->mysql->rowsReturned() == 0) { // Duplicated
			return WRONG_PASSWORD;
		}
//		print_r($userinfo); die($sql);
			
		$sql = "UPDATE ".$this->usr_tb." SET pwd=PASSWORD('".$this->convert->myEncrypt($userinfo['passwordsignup'])."') WHERE uid=".$this->convert->myDecrypt(base64_decode($_SESSION['2376fa']));
//		die($sql);
		$this->mysql->query($sql) or die($this->mysql->getErrors());
		
		
		return SUCCESS;
	}
}

?>