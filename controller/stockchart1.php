<?php
class StockChart {
	public function show($symbol){
		echo '
			<div class="container back-white">
				<div class="container-fluid">
					<div class="row-fluid">
						<div class="span8">
						<!-- main content part -->
							<form class="form-search" onsubmit="return false">
							<div class="info-part">
								<p class="subtitle">Stock, Index, ETF Symbol Info</p>
								<div class="subcontents">
									<div class="yui3-skin-sam"><!--DIV FOR AUTOCOMPLETE-->
										<input type="text" id="txtSymbol" class="input-medium" value="'.$symbol.'" name="txtSymbol" placeholder="Symbol" autocomplete="off">
										<button type="submit" id="';
		if($_SESSION['2376fa'] != "")
			echo 'btnScan';
		else
			echo 'btnBackhome';
		echo '" class="btn btn-info custom-font16">SCAN</button>
										<span id="symbolName" class="symbol-name"></span>
									</div>
								   <div class="content-2">
										<div class="row-fluid">
											<div class="span12">
												<span class="custom-bold18 padding15" id="prevClose"></span>
												<span id="intradayChange" class="padding15"><span id="change" class="custom-bold18"></span><span id="changePercent" class="custom-bold18"></span></span>
												<span class="custom-italic">52Wk Range:&nbsp;</span><span id="yearRange" class="custom-bold18"></span>
											</div>
										</div>
										<div class="row-fluid">
											<div class="span8">
												<span class="custom-italic">Intraday Price Quote Updated @ Market Close,</span>
												<span id="closeDate" class="custom-bold16"></span>
											</div>
										</div>
								   </div>
								</div>
							</div>
							<div class="chart-part">
								<p class="subtitle">Chart setting</p>
								<div class="chart-setting">
									<select id="ChartType" class="select-border">
										<option value="CandleStick">Candle Stick Chart</option>
										<option value="HiLow" selected>High Low Bar Chart</option>
										<option value="Line">Line (Closing Price) Chart</option>
										<option value="TrendBars">Trend Bar Chart</option>
									</select>
									<select id="dateRange" class="input-medium select-border">
										<option value="1" selected>1 Month</option>
										<option value="2">2 Months</option>
										<option value="3">3 Months</option>
										<option value="4">4 Months</option>
										<option value="6">6 Months</option>
										<option value="9">9 Months</option>
										<option value="12">12 Months</option>
										<option value="18">18 Months</option>
										<option value="24">24 Months</option>
									</select> 
								</div>
								<div id="chartDiv"></div>
							</div>
							</form>
							<div class="news-detail-part">
								<p class="subtitle">Tweets/News Detail</p>
								<div class="subcontents" id="news_detail"></div>
							</div>
							
						</div>
						<div class="span4">
						<!-- news content part -->
							<div class="info-part">
								<p class="subtitle">RSS News</p>
								<div class=body>
									<ul class="nav nav-tabs" id="myTab">
										<li><a href="#tweets_pannel">Tweets</a></li>
										<li class="active"><a href="#news_pannel">Yahoo News</a></li>
									</ul>
									<div class="tab-content">
										<div class="tab-pane" id="tweets_pannel" style="margin-left: 2px; margin-right: 2px; margin-top: 2px; margin-bottom: 2px;"></div>
										<div class="tab-pane active" id="news_pannel" style="margin-left: 2px; margin-right: 2px; margin-top: 2px; margin-bottom: 2px;"></div>
									</div>
									<script>
										$(\'#myTab a\').click(function (e) {
											e.preventDefault();
											$(this).tab(\'show\');
										})
									</script>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		';
	}
}
?>