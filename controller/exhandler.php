<?php
session_start();

class ExceptionHandler {
	
	public function getErrMsg(){
		return $_SESSION['err_msg'];
	}
	
	public function showMsgDiag() {
		echo '
		   <script src="http://code.jquery.com/jquery-1.9.1.js"></script>
		  <link rel="stylesheet" href="http://code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css" />
		  <script src="http://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>
			<script>
			$(function() {
				$("#dialog" ).dialog({
				  width: 500,
				  autoOpen: false,
				  show: {
					effect: "blind",
					duration: 1000
				  },
				  hide: {
					effect: "blind",
					duration: 1000
				  }
				}); 
				$( "#dialog" ).dialog( "open" );
			});
			</script>
			
			<div id="dialog" title="Note: TwitterStockResearch ">
			<p>'.$_SESSION['err_msg'].'</p>
			</div>
		';
		
		$_SESSION['neededToShowError'] = false;
	}
	
	public function needToShowErrMsg($msg) {
		$_SESSION['err_msg'] = $msg;
		
		$_SESSION['neededToShowError'] = true;
		
	}
	
	public function neededToShowMsg() {
		return $_SESSION['neededToShowError'];
	}
}
?>
