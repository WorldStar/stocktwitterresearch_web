
var color_serial = [ "#266975","#ff0000"];
var color_base = ["#266975"];
var color_warr = "#266975";

var ohlc_init = new Array();
var chartDateRange;  // chart time tick interval
var retVal = new Object();  // return min/max of date and value


// ------------   Get Time Range and value min/max -------------------
function getTimeAndValRange(ohlc)
{
	var min=0, max=0;
	var s_time, e_time;
	var arr_len = ohlc.length;
	
	$.each(ohlc, function(i, value){
		if(i == 0)
		{
			e_time = value[0];
			min = value[3];
		}
		if(i == (arr_len - 1))
		{
			s_time = value[0];
		}
		if(value[2] > max)
		{
			max = value[2];
		}
		if(value[3] < min)
		{
			min = value[3];
		}
	});
	var s_temp = new Date();
	var  kk = s_time.split('-');
	s_temp.setFullYear(kk[0], (kk[1] - 1), kk[2]);
	s_temp.setDate(s_temp.getDate() - 3);
	s_temp = s_temp.getFullYear() + "-" + (s_temp.getMonth()+1) + "-"  + s_temp.getDate();

	var e_temp = new Date();
	kk = e_time.split('-');
	e_temp.setFullYear(kk[0], (kk[1] - 1), kk[2]);
	e_temp.setDate(e_temp.getDate()+1);
	e_temp = e_temp.getFullYear() + "-" + (e_temp.getMonth()+1) + "-"  + e_temp.getDate();

	retVal.min = min;
	retVal.max = max;
	retVal.sTime = s_temp;
	retVal.eTime = e_temp;
}


// ------------   Get Time Range and value min/max End -------------------


function getTrendData(data){
	var ohlc_data = new Array();
	var ohlc_t_1 = new Array();
	var iState = 0;
	var len = data.length;

	if(data[1][4] < data[0][1])
	{
		color_serial.reverse();
		iState = 1;
	}

	for(var i = 0; i < (data.length); i++)
	{
		if(i  >= (data.length - 1))
		{
			ohlc_t_1.push(data[i]);
			break;
		}

		if((data[i+1][4] < data[i][1]) && (iState == 0))
		{
			if(i == 1)
			{
				ohlc_t_1.push(data[i]);			
			}
			ohlc_data.push(ohlc_t_1);
			iState = 1;
			ohlc_t_1 = new Array();
			ohlc_t_1.push(data[i]);
		}
		else if((data[i+1][4] >= data[i][1]) && (iState == 1))
		{
			if(i == 1)
			{
				ohlc_t_1.push(data[i]);			
			}			
			ohlc_data.push(ohlc_t_1);
			iState = 0;
			ohlc_t_1 = new Array();
			ohlc_t_1.push(data[i]);
		}
		else 
		{
			ohlc_t_1.push(data[i]);
		}
	}
	if(ohlc_t_1.length != 0)
	{
		ohlc_data.push(ohlc_t_1);
	}

	return ohlc_data;
}

function getLineDara(stock_array, time_range)
{
	var res_arr = new Array;
	jQuery.map(stock_array, function(value){
		res_arr.push([value[0], parseFloat(value[4]),value[5]]);
	});
  return res_arr;
}
// --  Draw candleStick ----------------
function candleStick_draw(ohlc){
  var plot2 = $.jqplot('chartDiv',[ohlc],{
   	seriesColors: color_base,
	seriesDefaults:{
		yaxis:'yaxis',
		markerOptions:{
			size: 6,
			color: '#ff0000'
		}
	},
    axes: {
      xaxis: {
        renderer:$.jqplot.DateAxisRenderer,
        tickOptions:{formatString:'%b %e %Y'},
		min: retVal.sTime,
		max: retVal.eTime
      },
      yaxis: {
		min: retVal.min,
		max: retVal.max,
        tickOptions:{formatString:'$%.1f'}
      }
    },
    // To make a candle stick chart, set the "candleStick" option to true.
    series: [
      {
        renderer:$.jqplot.OHLCRenderer, 
        rendererOptions:{ candleStick:true }
      }
    ],
    highlighter: {
      show: true,
      tooltipAxes: 'xy',
	  tooltipLocation:'n',
      yvalues: 5,
      formatString:'<table class="jqplot-highlighter"> \
      <tr><td>date:</td><td>%s</td></tr> \
      <tr><td>open:</td><td>%s</td></tr> \
      <tr><td>hi:</td><td>%s</td></tr> \
      <tr><td>low:</td><td>%s</td></tr> \
      <tr><td>close:</td><td>%s</td></tr> \
	  <tr><td>change:</td><td  style="color:#00A452">%s</td></tr></table>',
	  tooltipContentEditor: function(str, seriesIndex, pointIndex, plot){
		  if(str.indexOf('style="color:#00A452">-') != -1)
		  {
			  str = str.replace("#00A452","#ff0000");
		  }
		  return str;
		}
    }
  });
  plot2.replot();
}
// --  Draw High Low  ----------------
function HiLow_draw(ohlc){
	

  var plot2 = $.jqplot('chartDiv',[ohlc],{

	seriesColors: color_base,
	seriesDefaults:{
		yaxis:'yaxis',
		markerOptions:{
			size: 6,
			color: '#ff0000'
		}
	},
    axes: {
      xaxis: {
        renderer:$.jqplot.DateAxisRenderer,
        tickOptions:{formatString:'%b %e %Y'},
  		min: retVal.sTime,
		max: retVal.eTime
      },
      yaxis: {
		min: retVal.min,
		max: retVal.max,
        tickOptions:{formatString:'$%.1f'}
      }
    },
    // To make a candle stick chart, set the "candleStick" option to true.
    series: [
      {
        renderer:$.jqplot.OHLCRenderer 
      }
    ],
    highlighter: {
      show: true,
      tooltipAxes: 'xy',
	  tooltipLocation:'n',
      yvalues: 5,
      formatString:'<table class="jqplot-highlighter"> \
      <tr><td>date:</td><td>%s</td></tr> \
      <tr><td>open:</td><td>%s</td></tr> \
      <tr><td>hi:</td><td>%s</td></tr> \
      <tr><td>low:</td><td>%s</td></tr> \
      <tr><td>close:</td><td>%s</td></tr> \
	  <tr><td>change:</td><td  style="color:#00A452">%s</td></tr></table>',
	  tooltipContentEditor: function(str, seriesIndex, pointIndex, plot){
		  if(str.indexOf('style="color:#00A452">-') != -1)
		  {
			  str = str.replace("#00A452","#ff0000");
		  }
		  return str;
		}
	}
  });
    plot2.replot();

}

// --  Draw close Line  ----------------

function Line_draw(ohlc){
  var line_data =   getLineDara(ohlc,"");
  var plot2 = $.jqplot('chartDiv',[line_data],{
	seriesColors: color_base,
	seriesDefaults:{
		yaxis:'yaxis',
		markerOptions:{
			size: 0,
			color: '#ff0000'
		},
		rendererOptions: {

		}
	},
    axes: {
      xaxis: {
        renderer:$.jqplot.DateAxisRenderer,
        tickOptions:{formatString:'%b %e %Y'}, 
 		min: retVal.sTime,
		max: retVal.eTime
      },
      yaxis: {
		min: retVal.min,
		max: retVal.max,
        tickOptions:{formatString:'$%.1f'}
      }
    },

    highlighter: {
      show: true,
	  sizeAdjust: 10,
      tooltipAxes: 'xy',
	  tooltipLocation:'n',
      yvalues: 2,
      formatString:'<table class="jqplot-highlighter"> \
      <tr><td>date:</td><td>%s</td></tr> \
      <tr><td>close:</td><td>%s</td></tr> \
	  <tr><td>change:</td><td  style="color:#00A452">%s</td></tr></table>',
	  tooltipContentEditor: function(str, seriesIndex, pointIndex, plot){
		  if(str.indexOf('style="color:#00A452">-') != -1)
		  {
			  str = str.replace("#00A452","#ff0000");
		  }
		  return str;
		}
    }
  });
  plot2.replot();
}

function TrendBar(ohlc_init){

  var ohlc = new Array();
  ohlc = getTrendData(ohlc_init);
  
  var plot2 = $.jqplot('chartDiv',ohlc,{
    seriesColors: color_serial,
    seriesDefaults:{
		yaxis:'yaxis',
		renderer: $.jqplot.OHLCRenderer,
		markerOptions:{
			size: 6,
			color: '#ff0000'
		}
	},
    axes: {
      xaxis: {
        renderer:$.jqplot.DateAxisRenderer,
        tickOptions:{formatString:'%b %e %Y'}, 
		min: retVal.sTime,
		max: retVal.eTime
      },
      yaxis: {
		min: retVal.min,
		max: retVal.max,
        tickOptions:{formatString:'$%.1f'}
      }
    },
    // To make a candle stick chart, set the "candleStick" option to true.
    series: [
      {
        renderer:$.jqplot.OHLCRenderer 
      }
    ],
    highlighter: {
      show: true,
      tooltipAxes: 'xy',
	  tooltipLocation:'n',
      yvalues: 5,
      formatString:'<table class="jqplot-highlighter"> \
      <tr><td>date:</td><td>%s</td></tr> \
      <tr><td>open:</td><td>%s</td></tr> \
      <tr><td>hi:</td><td>%s</td></tr> \
      <tr><td>low:</td><td>%s</td></tr> \
      <tr><td>close:</td><td>%s</td></tr> \
	  <tr><td>change:</td><td  style="color:#00A452">%s</td></tr></table>',
	  tooltipContentEditor: function(str, seriesIndex, pointIndex, plot){
		  if(str.indexOf('style="color:#00A452">-') != -1)
		  {
			  str = str.replace("#00A452","#ff0000");
		  }
		  return str;
		}
    }
  });
  plot2.replot();
}
// Get Chart Type

function getChartType() {
	var chartType;
	chartType = $('#ChartType').val();
	return chartType;
}

/*ohlc_kk =[['2013-07-01',25.26,25.27,25.26,25.26,'-10(-10%)'],
['2013-06-28',25.43,25.54,24.89,25.13,'+10(+10%)'],
['2013-06-27',25.47,25.98,25.44,25.47,'+15(+15%)'],
['2013-06-26',25.22,25.68,25.01,25.29,'-9(-9%)'],
['2013-06-25',24.29,25.01,24.23,24.96,'-9(-9%)'],
['2013-06-24',24.98,25.09,23.82,24.07,'-9(-9%)'],
['2013-06-21',25.29,25.43,24.94,25.19,'+8(+8%)'],
['2013-06-20',26.03,26.05,25.23,25.35,'-7(-7%)']
];

ohlc_kk_l =[['2013-07-01',25.26,'-10(-10%)'],
['2013-06-28',25.13,'+10(+10%)'],
['2013-06-27',25.47,'+15(+15%)'],
['2013-06-26',25.29,'-9(-9%)'],
['2013-06-25',24.96,'-9(-9%)'],
['2013-06-24',24.07,'-9(-9%)'],
['2013-06-21',25.19,'+8(+8%)'],
['2013-06-20',25.35,'-7(-7%)']
];*/
