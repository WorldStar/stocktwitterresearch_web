// global chart parameter
var g_ohlc;
// get total data
function getAllData(sid,m){
	getQuoteData(sid);
	getHistoricalData(sid,m);
	getNewsFeedData(sid);
}

// get quote data from csv
function getQuoteData(sid){
	$.get("quotecsv.php",{ sid:sid}, function(data,status){
		data = preprocessData(data);
		var quoteArray = getQuoteArrayData(data);

		$('#symbolName').text(quoteArray[1]);
		$("#prevClose").text((quoteArray[2] == 0)?"N/A":quoteArray[2]);// add Previous Last Close
		
		// change text color(green/red)change according to daily change Up and Down state
		if(getUpDownChange(quoteArray[3])){
			$("#intradayChange").removeClass("intradayChangeDown")
			$("#intradayChange").addClass("intradayChangeUp"); // color green
		} else {
			$("#intradayChange").removeClass("intradayChangeUp")
			$("#intradayChange").addClass("intradayChangeDown"); // color red
		}

		$("#change").text(quoteArray[3]);// add change

		var changePercentStr = "(" + quoteArray[4] + ")";
		$("#changePercent").text(changePercentStr);// add change percent,ex:(0.55%)

		$("#yearRange").text(quoteArray[5]);// add Year Range
		$("#closeDate").text(quoteArray[6]);// add Previous Last Close
	});
}

function getQuoteArrayData(data){
	var returnArray = new Array();
	var oarray = data.split("\r");

	var subArray = oarray[0].split(",");
	
	returnArray.push(
		getDoubleQuoteExtractString(subArray[0]),
		getDoubleQuoteExtractString(subArray[1]),
		subArray[2],
		subArray[3],
		getDoubleQuoteExtractString(subArray[4]),
		getDoubleQuoteExtractString(subArray[5]),
		getDoubleQuoteExtractString(subArray[6])
	);

	return returnArray;
}

// extract double quote from a string(ex. "Yahoo!inc")
function getDoubleQuoteExtractString(s){
	var firstStr = s.slice(0,1);

	if(firstStr == '"'){
		return s.substr(1,s.length-2);
	}

	return s;
}

// get historical data as CSV file
function getHistoricalData(sid,m){
	$.get("historycsv.php",{ sid:sid, drange:m }, function(data,status){
		data = preprocessData(data);
		g_ohlc = getOHLCData(data);

	setDrawChart(g_ohlc);
	});

}

// extract ad source from freehosting server
function preprocessData(data){
	var returnData;
	var to = data.indexOf("<!-- Free Web Hosting Area Start -->");

	switch(to){
		case 0:
		  returnData = data.substr(0,to);
		  break;
		case -1:
		  returnData = data;
		  break;
		default:
		  returnData = data.substr(0,to-1);
	}
	return returnData;
}

// Extract OHLC(open,high,low,close) data neccesary for chart drawing from csv
function getOHLCData(data){
	var ohlc = new Array();
	var oarray = data.split("\n");
	var count = oarray.length;

	for(var i=1;i<count-2;i++){
		var dayItem = oarray[i];
		var subArray = dayItem.split(",");

		var nextItem = oarray[i+1];
		var nextArray = nextItem.split(",");

		// Calculate change from previous day
		var changeFromPrev = getChangeFromPrev(subArray[4],nextArray[4]);
		ohlc.push([subArray[0],parseFloat(subArray[1]),parseFloat(subArray[2]),parseFloat(subArray[3]),parseFloat(subArray[4]),changeFromPrev]);
	}

	return ohlc;
}

// get change string at the chart tip

function getChangeFromPrev(cur,prev){
	var change = parseFloat(cur) - parseFloat(prev);
	change = change.toFixed(2);
	
	var changeinPercent = parseFloat((change*100)/prev);
	changeinPercent = changeinPercent.toFixed(2);
	
	var changeString = "";
	if(change >= 0){
		changeString = changeString + "+" + change;
		changeString = changeString + "(+" + changeinPercent + "%)";
	} else {
		changeString = changeString + change;
		changeString = changeString + "(" + changeinPercent + "%)";	
	}
	return changeString;
}

// get info if stock change is plus or minus
function getUpDownChange(ch){
	var chString = new String(ch);
	var chValue = Number(chString);

	if(chValue < 0){
		return false; // change minus = green color
	}

	return true; // change plus = red color
}

function getNewsFeedData(sid){
		$.ajax({
			type: 'get',
			url: 'get-tweets.php',
			data:{sid:sid},
			dataType: 'text',
			async: false,
			success: function ( xmlData ) {
				var rss_result='';
				$(xmlData).find('div.content').has('a[data-expanded-url]').each(function(index){
					var $item = $(this);
					
					var tweet_id=$item.find('a.js-details').attr('href').split('/');
					tweet_id=tweet_id[tweet_id.length-1];
					var rss_item=
					'<div class="rss-item-container"><div class="rss-item"><a class="rss-item-title tweet" href="'+
					$item.find('a[data-expanded-url]').attr('data-expanded-url')+
						'" data-avatar="'+$item.find('img.avatar').attr('src')+
						'" data-fullname="'+$item.find('strong.fullname').text()+
						'" data-username="'+$item.find('span.username').text()+
						'" data-userpage="https://twitter.com'+$item.find('a.account-group').attr('href')+'"'+
						'" data-tweetpage="https://twitter.com'+$item.find('a.js-details').attr('href')+'"'+
						'" data-time="'+$item.find('a.tweet-timestamp').attr('title')+
						'" data-tweetid="'+tweet_id+'">';
					var temp=$item.find('p.tweet-text').text();
					rss_item+=temp+'</a><div><i class="icon-file-alt item-icon"></i><a class="item-command" href="https://twitter.com'+
						$item.find('a.js-details').attr('href')+'" target="_blank">View Tweet</a>';
					rss_item+='<span class="item-right"><i class="icon-reply item-icon"></i><a class="item-command" href="https://twitter.com/intent/tweet?in_reply_to='+
						tweet_id+'">Reply</a>';
					rss_item+='<i class="icon-refresh item-icon"></i><a class="item-command" href="https://twitter.com/intent/retweet?tweet_id='+
						tweet_id+'">Retweet</a>';
					rss_item+='<i class="icon-star item-icon"></i><a class="item-command" href="https://twitter.com/intent/favorite?tweet_id='+
						tweet_id+'">Favorite</a></span>';
						rss_item+='</div></div></div>';
					rss_result+=rss_item;
				});
				$('#tweets_pannel').empty();
				$(rss_result).appendTo($('#tweets_pannel'));
				$('a.rss-item-title.tweet').click(function(){
					var detail_text='';
					detail_text+='<div class="news-detail-header">'
						detail_text+='<div class="avatar"><a href="'+$(this).attr('data-userpage')+
							'" target="_blank"><img src="'+$(this).attr('data-avatar')+'"></a></div>';
						detail_text+='<div class="personality">';
							detail_text+='<strong>'+$(this).attr('data-fullname')+'</strong> ';
							detail_text+=' <span>'+$(this).attr('data-username')+'</span><br>';
							detail_text+='<p class="pub-time"><i>'+$(this).attr('data-time')+'</i></p>';
						detail_text+='</div>';
					detail_text+='</div>';
					detail_text+='<blockquote>'+$(this).text()+'</blockquote>';
					detail_text+='<div>';
						detail_text+='<i class="icon-file-alt item-icon"></i><a class="item-command" href="'+
							$(this).attr('data-tweetpage')+'" target="_blank">View Tweet</a>';
						detail_text+='<span class="item-right" style="display:block;">';
							detail_text+='<i class="icon-reply item-icon"></i><a class="item-command" href="https://twitter.com/intent/tweet?in_reply_to='+
								$(this).attr('data-tweetid')+'">Reply</a>'
							detail_text+='<i class="icon-refresh item-icon"></i><a class="item-command" href="https://twitter.com/intent/retweet?tweet_id='+
								$(this).attr('data-tweetid')+'">Retweet</a>'
							detail_text+='<i class="icon-star item-icon"></i><a class="item-command" href="https://twitter.com/intent/favorite?tweet_id='+
								$(this).attr('data-tweetid')+'">Favorite</a>'
						detail_text+='</span>';
					detail_text+='</div>';
					$('#news_detail').empty();
					$(detail_text).appendTo($('#news_detail'));
					window.scrollTo(0,400);
					return false;
				});
			}
		});
		
		$.ajax({
			type: 'get',
			url: 'get-rss.php',
			data:{sid:sid},
			dataType: 'text',
			async: false,
			success: function ( xmlData ) {
				xmlData=xmlData.replace(/<link>/g, '<linka>');
				xmlData=xmlData.replace(/<\/link>/g, '</linka>');
				var rss_result='';
				var count=0;
				
				var initDetail = ''; //===========
				$(xmlData).find('Item').each(function(index) {
					
					var $item = $(this);
					var rss_item=
					'<div class="rss-item-container">'+
						'<div class="rss-item">'+
							'<a class="rss-item-title yahoo" href="'+$item.find('linka').text()+'" '+
								'data-desc="'+$item.find('description').text().replace(/\"/g,"'")+'" '+
								'data-time="'+$item.find('pubDate').text()+'">'+
								$item.find('title').text()+
							'</a>'+
						'<div style="float:left;">'+
						'<i class="icon-file-alt item-icon"></i>'+
						'<a class="item-command" href="'+$item.find('linka').text()+'" target="_blank">View News</a>'+
					'</div>';
					rss_item+='<div class="item-right"><a href="https://twitter.com/share" class="twitter-share-button" data-url="'+
						$item.find('linka').text()+'" data-text="'+
						$item.find('title').text().replace(/\"/g,'\'')+'">Tweet</a></div>'
					rss_item+='</div></div>';
					rss_result+=rss_item;
				});
				$('#news_pannel').empty();
				$(rss_result).appendTo($('#news_pannel'));
				!function(d,s,id){
					var js, p=/^http:/.test(d.location)?'http':'https';
					js=d.createElement(s);
					js.src=p+'://platform.twitter.com/widgets.js';
					$(js).appendTo($('#news_pannel'));
				}(document, 'script', 'twitter-wjs');
				
				$('a.rss-item-title.yahoo').click(function(){
					var detail_text='';
					detail_text+='<div class="news-detail-header">'
						detail_text+=
						'<div class="avatar">'+
							'<img src="https://si0.twimg.com/profile_images/2622681169/mxl6j2bkzyrcyyco1d8x_normal.jpeg">'+
						'</div>';
						detail_text+='<div class="personality">';
							detail_text+='<strong>'+$(this).text()+'</strong>';
							detail_text+='<p class="pub-time"><i>'+$(this).attr('data-time')+'</i></p>';
						detail_text+='</div>';
					detail_text+='</div>';
					detail_text+='<blockquote>'+$(this).attr('data-desc')+'</blockquote>';
					detail_text+='<div>';
						detail_text+='<i class="icon-file-alt item-icon"></i><a class="item-command" href="'+
							$(this).attr('href')+'" target="_blank">View News</a>';
						detail_text+='<div class="item-right" style="display:block;">';
							detail_text+='<a href="https://twitter.com/share" class="twitter-share-button" data-url="'+
								$(this).attr('href')+'" data-text="'+$(this).text().replace(/\"/g,"'")+'">Tweet</a>';
						detail_text+='</div>';
					detail_text+='</div>';
					$('#news_detail').empty();
					$(detail_text).appendTo($('#news_detail'));
					!function(d,s,id){
						var js, p=/^http:/.test(d.location)?'http':'https';
						js=d.createElement(s);
						js.src=p+'://platform.twitter.com/widgets.js';
						$(js).appendTo($('#news_pannel'));
					}(document, 'script', 'twitter-wjs');
					window.scrollTo(0,400);
					return false;
				});
				$($('a.rss-item-title.yahoo')[0]).click();
			}
		});
		
	window.scrollTo(0,0);
}

// draw chart
function setDrawChart(ohlc){
	var chartType = getChartType();

	getTimeAndValRange(ohlc);

	switch(chartType)
	{
		case 'CandleStick':
			candleStick_draw(ohlc);
			break;
		case 'HiLow':
			HiLow_draw(ohlc);
			break;
		case 'Line':
			Line_draw(ohlc);
			break;
		case 'TrendBars':
			TrendBar(ohlc);
			break;
		default:
		    Line_draw(ohlc);
			break;
	}
}

function getCheckInputSymbol(symbol){
	if(symbol == null || symbol == ""){
		return false;
	}
	
	return true;
}


function displayInfoProcess(symbol){

	var drange = $("#dateRange").val();
	getAllData(symbol,drange);
}

function refreshHistoricalData(){
	var symbol = $("#txtSymbol").val();
	var drange = $("#dateRange").val();

	if(!getCheckInputSymbol(symbol)){
		return false;
	}

	getHistoricalData(symbol,drange);
}

function refreshChartType(){
	setDrawChart(g_ohlc);
}

$(document).ready(function(){
	displayInfoProcess($('#txtSymbol').val());
	
	// scan button click process
	$('#btnScan').click(function(){
		var strSymbol = $("#txtSymbol").val();
//		alert("dfj");
		displayInfoProcess(strSymbol);
	});
	
	$('#btnBackhome').click(function(){
		alert("Please kindly log in to continue.");
		window.location.href = "index.php";
	});
	
	// chart type change process
	$("#ChartType").change(function(){					
			refreshChartType();
	});
	$("#ReturnChartType").change(function(){					
		alert("Please kindly log in to continue.");
		window.location.href = "index.php";
	});
	
	// date range change process
	$("#dateRange").change(function(){
		refreshHistoricalData();
	});
	$("#ReturndateRange").change(function(){				
		alert("Please kindly log in to continue.");
		window.location.href = "index.php";
	});
	
	// input symbol enter key process
	$('#txtSymbol').keypress(function(event){
		var keycode = (event.keyCode ? event.keyCode : event.which);
		if(keycode == '13'){
			document.getElementById('btnScan').focus();
			if(!getCheckInputSymbol(symbol)){
					return false;
			}
			var strSymbol = $('#txtSymbol').val();
			displayInfoProcess(strSymbol);
		}
		event.stopPropagation();
	});

	// login script Simon 2013-09-08
	$("#cancel_hide").click(function(){
		$("#login_form").fadeOut("normal");
		$("#shadow").fadeOut();
	});
	$("#login").click(function(){
	
		username=$("#user_name").val();
		password=$("#password").val();
		 $.ajax({
			type: "POST",
			url: "login.php",
			data: "name="+username+"&pwd="+password,
			success: function(html){
			  if(html=='true')
			  {
				$("#login_form").fadeOut("normal");
				$("#shadow").fadeOut();
				$("#profile").html("<a href='logout.php' id='logout'>Logout</a>");
				
			  }
			  else
			  {
					$("#add_err").html("Wrong username or password");
			  }
			},
			beforeSend:function()
			{
				 $("#add_err").html("Loading...")
			}
		});
		 return false;
	});
});