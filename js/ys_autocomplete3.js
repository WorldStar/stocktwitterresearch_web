/*global YUI, YAHOO:true, alert */

"use strict";

var YAHOO = {};
YAHOO.util = {};
YAHOO.util.ScriptNodeDataSource = {};

YUI({
    filter: "raw"
}).use("datasource", "autocomplete", "highlight", function (Y) {
    var oDS, acNode = Y.one("#txtSymbol");

    oDS = new Y.DataSource.Get({
        source: "http://d.yimg.com/aq/autoc?query=",
        generateRequestCallback: function (id) {
            YAHOO.util.ScriptNodeDataSource.callbacks = YUI.Env.DataSource.callbacks[id];
            return "&callback=YAHOO.util.ScriptNodeDataSource.callbacks";
        }
    });
    oDS.plug(Y.Plugin.DataSourceJSONSchema, {
        schema: {
            resultListLocator: "ResultSet.Result",
            resultFields: ["symbol", "name", "exch", "type", "exchDisp"]
        }
    });

    acNode.plug(Y.Plugin.AutoComplete, {
        maxResults: 10,
        resultTextLocator: "symbol",

		//////////////////////////////////////////////////////////////////
		//////////////////////////////////////////////////////////////////
		align: {
			  node  : '#txtSymbol',
			  points: [Y.WidgetPositionAlign.TL, Y.WidgetPositionAlign.BL]
		},
		//////////////////////////////////////////////////////////////////
		//////////////////////////////////////////////////////////////////

        resultFormatter: function (query, results) {
            return Y.Array.map(results, function (result) {
                var asset = result.raw,
                    text = asset.symbol + " " + asset.name;

                return Y.Highlight.all(text, query);
            });
        },
        requestTemplate: "{query}&region=US&lang=en-US",
        source: oDS
    });

    acNode.ac.on("select", function (e) {
//        alert(e.result.raw.name);
    });
});

function verify_changepass() {
		if(document.getElementById("oldpassword").value.length < 8) {
			alert("Password must be longer than 8.");
			document.getElementById("oldpassword").focus();
			return;
		}
		
		if(document.getElementById("passwordsignup").value.length < 8) {
			alert("Password must be longer than 8.");
			document.getElementById("passwordsignup").focus();
			return;
		}
		
		if(document.getElementById("passwordsignup").value != document.getElementById("passwordsignup_confirm").value) {
			alert("Password was worng. Try again.");
			document.getElementById("passwordsignup_confirm").focus();
			return;
		}
		
		document.getElementById("changepass_frm").submit();
}	

function verify_signup() {
		if(document.getElementById("usernamesignup").value.length < 3) {
			alert("User name must be longer than 3.");
			document.getElementById("usernamesignup").focus();
			return;
		}
		
		if(document.getElementById("emailsignup").value.indexOf("@") <= 0 || document.getElementById("emailsignup").value.indexOf(".") <= 3) {
			alert("Correct email is required.");
			document.getElementById("emailsignup").focus();
			return;
		}
		
		if(document.getElementById("passwordsignup").value.length < 8) {
			alert("Password must be longer than 8.");
			document.getElementById("passwordsignup").focus();
			return;
		}
		
		if(document.getElementById("passwordsignup").value != document.getElementById("passwordsignup_confirm").value) {
			alert("Password was worng. Try again.");
			document.getElementById("passwordsignup_confirm").focus();
			return;
		}
		
		document.getElementById("signup_frm").submit();
}	
