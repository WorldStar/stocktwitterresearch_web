<?php
/**************************************************************/
/*         Matt secure version 1.0 beta (15/09/13)            */
/*              Copyright 2013 twitterstockresearch.com, Inc. */
/*                                                            */
/*          ALWAYS CHECK FOR THE LATEST RELEASE AT            */
/*              http://www.twitterstockresearch.com           */
/*                                                            */
/*                                                            */
/**************************************************************/
/*           Developer: future                                */
/**************************************************************/
require_once("config.php");
class Encryption {
    public  function safe_b64encode($string) {
        $data = base64_encode($string);
        $data = str_replace(array('+','/','='),array('-','_',''),$data);
        return $data;
    }
 
 
    public  function encode($value){ 
	    $skey = SKEY;
        if(!$value){return false;}
        $text = $value;
        $iv_size = mcrypt_get_iv_size(MCRYPT_RIJNDAEL_256, MCRYPT_MODE_ECB);
		
        $iv = mcrypt_create_iv($iv_size, MCRYPT_RAND);
        $crypttext = mcrypt_encrypt(MCRYPT_RIJNDAEL_256, $this->skey, $text, MCRYPT_MODE_ECB, $iv);
        return trim($this->safe_b64encode($crypttext)); 
    }
 
}
?>