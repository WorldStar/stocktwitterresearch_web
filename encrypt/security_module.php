<?php
/**************************************************************/
/*         Matt secure version 1.0 beta (15/09/13)            */
/*              Copyright 2013 twitterstockresearch.com, Inc. */
/*                                                            */
/*          ALWAYS CHECK FOR THE LATEST RELEASE AT            */
/*              http://www.twitterstockresearch.com           */
/*                                                            */
/*                                                            */
/**************************************************************/
/*           Developer: future                                */
/**************************************************************/
class Security{
  function __construct( ){
  }
  // Encrypt function
  public function myEncrypt($value){
 	 $convert1 = new Encryption();
 	 $response = $convert1->encode($value);
	 $convert2 = new Encryption_2();
	 $result = $convert2->encryptIt($response);
     return $result;
  }
  //Decrypt function
  public function myDecrypt($value){
	 $convert2 = new Decryption_2();
	 $response = $convert2->decryptIt($value);
 	 $convert1 = new Decryption();
 	 $result = $convert1->decode($response);
    return $result;
  }
}
?>