<?php
session_start(); 

include_once("conf/definition.php");
include_once("model/mysql.lib.php");
include_once("controller/navigator.php");
include_once("controller/stockchart.php");
include_once("controller/exhandler.php");
//include_once("controller/usermng.php");
include_once("view/home.php");
include_once("view/login.php");
include_once("view/footer.php");
include_once("view/header.php");
include_once("view/profile.php");


?>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<title>TwitterStockResearch</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0">

<link rel="stylesheet" href="css/bootstrap.css">
<link rel="stylesheet" href="css/bootstrap-responsive.css">
<link rel="stylesheet" type="text/css" href="css/jquery.jqplot.css" />
<link rel="stylesheet" type="text/css" href="syntaxhighlighter/styles/shCoreDefault.css" />
<link rel="stylesheet" type="text/css" href="syntaxhighlighter/styles/shThemejqPlot.css" />
<link href="css/bootstrap-combined.no-icons.css" rel="stylesheet">
<link href="//netdna.bootstrapcdn.com/font-awesome/3.2.1/css/font-awesome.css" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="css/style.css">

<!-- Login -->
<link rel="stylesheet" type="text/css" href="./css/login/style.css" />
<link rel="stylesheet" type="text/css" href="./css/login/animate-custom.css" />
<!----------->

<!-- Dialog -->
<link rel="stylesheet" href="http://code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css" />
<!------------>

<?php
if(!empty($_POST) || isset($_SESSION['2376fa']))
	echo '<script src="js/jquery.js"></script>';
//print_r($_POST); die($_SESSION['login']);

?>

<script src="js/excanvas.js"></script>
<script src="js/jquery.jqplot.js"></script>

<script src="plugins/jqplot.dateAxisRenderer.js"></script>
<script src="plugins/jqplot.ohlcRenderer.js"></script>
<script src="plugins/jqplot.highlighter.js"></script>

<script src="syntaxhighlighter/scripts/shCore.js"></script>
<script src="syntaxhighlighter/scripts/shBrushJScript.js"></script>
<script src="syntaxhighlighter/scripts/shBrushXml.js"></script>

<script src="js/bootstrap.js"></script>
<script src="js/yui.js"></script>
<?
  //if(isset($_SESSION['2376fa']))
     echo '<script src="js/ys_autocomplete.js"></script>';
?>


<?php
//print_r($_REQUEST);print_r($_POST);

//if(empty($_POST) && isset($_SESSION['2376fa']) && !isset($_REQUEST['profile']))
if(!isset($_REQUEST['profile']))
	echo '
		<script src="js/yql_process.js"></script>
		<script src="js/chart_module.js"></script>
		';
?>
</head>
<body>


<?php

if(ExceptionHandler::neededToShowMsg())
	ExceptionHandler::showMsgDiag();

Header::show();

if(empty($_POST) && !isset($_SESSION['2376fa'])) {
	$_SESSION['l94jso28dn'] = "";
	Home::show();
}
else {
	
	if(isset($_SESSION['l94jso28dn'])) {
		unset($_SESSION['l94jso28dn']);
		Navigator::redirect("index.php");
	}
	
//	print_r($_SESSION);die(isset($_GET['profile']));
	if(isset($_GET['profile'])) {
		$profileview = new Profile();
		$profileview->show();
	} 
	else {
	    //if(isset($_SESSION['2376fa']))
			$symbol = isset($_POST['txtSymbol']) ? $_POST['txtSymbol'] : 'YHOO';
		//else 
		    //$symbol = isset($_POST['ttSymbol']) ? $_POST['ttSymbol'] : 'YHOO';
		StockChart::show($symbol);
	}
	
}

Footer::show();
?>
</body>
</html>